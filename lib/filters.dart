import 'dart:io' ;
import 'package:image/image.dart' ;



File protanopia(String path, File _image)   {
  var file = _image;
  var x = decodeImage(file.readAsBytesSync());
  var y = colorSaturationProtanopia(x, 0.8);
  var s = encodePng(y);
  var p = new File(path + '/protanopia.png')..writeAsBytesSync(s);
  return  p;
}

File deuteranopia(String path, File _image)   {
  var file = _image;
  var x = decodeImage(file.readAsBytesSync());
  var y = colorSaturationDeuteranopia(x,0.8,1);
  var s = encodePng(y);
  var p = new File(path + '/deuteranopia.png')..writeAsBytesSync(s);
  return p;
}

File tritanopia(String path, File _image)   {
  var file = _image;
  var x = decodeImage(file.readAsBytesSync());
  var y = colorSaturationTritanopia(x, 1);
  var s = encodePng(y);
  var p = new File(path + '/tritanopia.png')..writeAsBytesSync(s);
  return p;
}

File monocromaticoAzul(String path, File _image)   {
  var file = _image;
  var x = decodeImage(file.readAsBytesSync());
  var y = colorMonocromaticoAzul(x);
  var s = encodePng(y);
  var p = new File(path + '/monocromaticoAzul.png')..writeAsBytesSync(s);
  return p;
}

File acromatico(String path, File _image)   {
  var file = _image;
  var x = decodeImage(file.readAsBytesSync());
  var y = grayscale(x);
  var s = encodePng(y);
  var p = new File(path + '/acromatico.png')..writeAsBytesSync(s);
  return p;
}
Image colorSaturationDeuteranopia(Image src, double valueRED,double valueGREEN) {
  var pixels = src.getBytes();

  for (int i = 0, len = pixels.length; i < len; i += 4) {
    var R = pixels[i];
    var G = pixels[i + 1];
    var B = pixels[i + 2];

    var L = 17.8824 * R + 43.5161 * G + 4.11935 * B;
    var M = 3.45565 * R + 27.1554 * G + 3.86714 * B;
    var S = 0.0299566 * R + 0.184309 * G + 1.46709 * B;

    var Lp = 1 * L + 0 * M + 0 * S;
    var Mp = 0.49421 * L + 0 * M + 1.24827 * S;
    var Sp = 0 * L + 0 * M + 1 * S;

    var Ri = ((0.0809444479 * Lp - 0.130504409 * Mp + 0.116721066 * Sp).floor());
    var Gi = ((-0.0102 * Lp + 0.0540 * Mp - 0.1136 * Sp).floor());
    var Bi = ((-0.000365296938 * Lp - 0.00412161469 * Mp + 0.693511405 * Sp).floor());


    pixels[i] = Ri;
    pixels[i + 1] = Gi;
    pixels[i + 2] = Bi;

  }
  return src;
}

Image colorSaturationProtanopia(Image src, double value) {
  var pixels = src.getBytes();

  for (int i = 0, len = pixels.length; i < len; i += 4) {
    var R = pixels[i];
    var G = pixels[i + 1];
    var B = pixels[i + 2];

    var L = 17.8824 * R + 43.5161 * G + 4.11935 * B;
    var M = 3.45565 * R + 27.1554 * G + 3.86714 * B;
    var S = 0.0299566 * R + 0.184309 * G + 1.46709 * B;

    var Lp = 0 * L + 2.02344 * M + -2.52581 * S;
    var Mp = 0 * L + 1 * M + 0 * S;
    var Sp = 0 * L + 0 * M + 1 * S;

    var Ri = ((0.0809444479 * Lp - 0.130504409 * Mp + 0.116721066 * Sp).floor());
    var Gi = ((-0.0102 * Lp + 0.0540 * Mp - 0.1136 * Sp).floor());
    var Bi = ((-0.000365296938 * Lp - 0.00412161469 * Mp + 0.693511405 * Sp).floor());

    pixels[i] = Ri;
    pixels[i + 1] = Gi;
    pixels[i + 2] = Bi;

  }
  return src;
}

Image colorMonocromaticoAzul(Image src) {
  var pixels = src.getBytes();

  for (int i = 0, len = pixels.length; i < len; i += 4) {
    var gray = 0.2989*pixels[i] + 0.5870 * pixels[i + 1] + 0.1140*pixels[i + 2];
    pixels[i] = clamp255((gray * 1 + pixels[i]*(1-1)).round());//red
    pixels[i + 1] = clamp255((gray * 1 + pixels[i]*(1-1)).round());//green
  }
  return src;
}

Image colorSaturationTritanopia(Image src, double value) {
  var pixels = src.getBytes();

  for (int i = 0, len = pixels.length; i < len; i += 4) {
    var R = pixels[i];
    var G = pixels[i + 1];
    var B = pixels[i + 2];

    var L = 17.8824 * R + 43.5161 * G + 4.11935 * B;
    var M = 3.45565 * R + 27.1554 * G + 3.86714 * B;
    var S = 0.0299566 * R + 0.184309 * G + 1.46709 * B;

    var Lp = 1 * L + 0 * M + 0 * S;
    var Mp = 0 * L + 1 * M + 0 * S;
    var Sp = -0.395913 * L + 0.801109 * M + 0 * S;

    var Ri = ((0.0809444479 * Lp - 0.130504409 * Mp + 0.116721066 * Sp).round());
    var Gi = ((-0.0102 * Lp + 0.0540 * Mp - 0.1136 * Sp).round());
    var Bi = ((-0.000365296938 * Lp - 0.00412161469 * Mp + 0.693511405 * Sp).round());
    if (Bi <= 0){
      Bi = 0;
    }



    pixels[i] = Ri;
    pixels[i + 1] = Gi;
    pixels[i + 2] = Bi;

  }
  return src;
}

int clamp255(int x) => x.clamp(0, 255);
