import 'dart:io' ;
import 'package:image/image.dart' ;


File protanopiaInv(String path, File _image)   {
  var file = _image;
  var x = decodeImage(file.readAsBytesSync());
  var y = colorSaturationProtanopia(x);
  var s = encodePng(y);
  var p = new File(path + '/protanopiaInv.png')..writeAsBytesSync(s);
  return  p;
}

File deuteranopiaInv(String path, File _image)   {
  var file = _image;
  var x = decodeImage(file.readAsBytesSync());
  var y = colorSaturationDeuteranopia(x);
  var s = encodePng(y);
  var p = new File(path + '/deuteranopiaInv.png')..writeAsBytesSync(s);
  return p;
}

File tritanopiaInv(String path, File _image)   {
  var file = _image;
  var x = decodeImage(file.readAsBytesSync());
  var y = colorSaturationTritanopia(x);
  var s = encodePng(y);
  var p = new File(path + '/tritanopiaInv.png')..writeAsBytesSync(s);
  return p;
}

Image colorSaturationDeuteranopia(Image src) {
  var pixels = src.getBytes();

  for (int i = 0, len = pixels.length; i < len; i += 4) {
    var R = pixels[i];
    var G = pixels[i + 1];
    var B = pixels[i + 2];

    var L = 17.8824 * R + 43.5161 * G + 4.11935 * B;
    var M = 3.45565 * R + 27.1554 * G + 3.86714 * B;
    var S = 0.0299566 * R + 0.184309 * G + 1.46709 * B;

    var Lp = 1 * L + 0 * M + 0 * S;
    var Mp = 0.49421 * L + 0 * M + 1.24827 * S;
    var Sp = 0 * L + 0 * M + 1 * S;

    var Ri = ((0.0809444479 * Lp - 0.130504409 * Mp + 0.116721066 * Sp).floor());
    var Gi = ((-0.0102 * Lp + 0.0540 * Mp - 0.1136 * Sp).floor());
    var Bi = ((-0.000365296938 * Lp - 0.00412161469 * Mp + 0.693511405 * Sp).floor());

    var Dr = R - Ri;
    var Dg = G - Gi;
    var Db = B - Bi;

    var Rm = 1 * Dr + 0.7 * Dg + 0 * Db;
    var Gm = 0 * Dr + 0 * Dg + 0 * Db;
    var Bm = 0 * Dr + 0.7 * Dg + 1 * Db;

    pixels[i] = ((R + Rm).round());
    pixels[i + 1] = ((G + Gm).round());
    pixels[i + 2] = ((B + Bm).round());

  }
  return src;
}

Image colorSaturationProtanopia(Image src) {
  var pixels = src.getBytes();

  for (int i = 0, len = pixels.length; i < len; i += 4) {
    var R = pixels[i];
    var G = pixels[i + 1];
    var B = pixels[i + 2];

    var L = 17.8824 * R + 43.5161 * G + 4.11935 * B;
    var M = 3.45565 * R + 27.1554 * G + 3.86714 * B;
    var S = 0.0299566 * R + 0.184309 * G + 1.46709 * B;

    var Lp = 0 * L + 2.02344 * M + -2.52581 * S;
    var Mp = 0 * L + 1 * M + 0 * S;
    var Sp = 0 * L + 0 * M + 1 * S;

    var Ri = ((0.0809444479 * Lp - 0.130504409 * Mp + 0.116721066 * Sp).floor());
    var Gi = ((-0.0102 * Lp + 0.0540 * Mp - 0.1136 * Sp).floor());
    var Bi = ((-0.000365296938 * Lp - 0.00412161469 * Mp + 0.693511405 * Sp).floor());

    var Dr = R - Ri;
    var Dg = G - Gi;
    var Db = B - Bi;

    var Rm = 0 * Dr + 0 * Dg + 0 * Db;
    var Gm = 0.7 * Dr + 1 * Dg + 0 * Db;
    var Bm = 0.7 * Dr + 0 * Dg + 1 * Db;

    pixels[i] = ((R + Rm).round());
    pixels[i + 1] = ((G + Gm).round());
    pixels[i + 2] = ((B + Bm).round());

  }
  return src;
}

Image colorSaturationTritanopia(Image src) {
  var pixels = src.getBytes();

  for (int i = 0, len = pixels.length; i < len; i += 4) {
    var R = pixels[i];
    var G = pixels[i + 1];
    var B = pixels[i + 2];

    var L = 17.8824 * R + 43.5161 * G + 4.11935 * B;
    var M = 3.45565 * R + 27.1554 * G + 3.86714 * B;
    var S = 0.0299566 * R + 0.184309 * G + 1.46709 * B;

    var Lp = 1 * L + 0 * M + 0 * S;
    var Mp = 0 * L + 1 * M + 0 * S;
    var Sp = -0.395913 * L + 0.801109 * M + 0 * S;

    var Ri = ((0.0809444479 * Lp - 0.130504409 * Mp + 0.116721066 * Sp).floor());
    var Gi = ((-0.0102 * Lp + 0.0540 * Mp - 0.1136 * Sp).floor());
    var Bi = ((-0.000365296938 * Lp - 0.00412161469 * Mp + 0.693511405 * Sp).floor());

    if (Bi <= 0){
      Bi = 0;
    }
    if (Gi <= 0){
      Gi = 0;
    }
    if (Ri <= 0){
      Ri = 0;
    }

    var Dr = R - Ri;
    var Dg = G - Gi;
    var Db = B - Bi;

    var Rm = 1 * Dr + 0 * Dg + 0.7 * Db;
    var Gm = 0 * Dr + 1 * Dg + 0.7 * Db;
    var Bm = 0 * Dr + 0 * Dg + 0 * Db;

    pixels[i] = ((R + Rm).round());
    pixels[i + 1] = ((G + Gm).round());
    pixels[i + 2] = ((B + Bm).round());

  }
  return src;
}

int clamp255(int x) => x.clamp(0, 255);
