import 'dart:io' ;

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'filters.dart' as filters;
import 'filtersInv.dart' as filtersInv;



void main() => runApp(new MaterialApp(home: MyApp()));

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  File _image;
  File _oimage;
  //normales
  File _protanopia;
  File _deuteranopia;
  File _tritanopia;
  File _monocromaticoAzul;

  //correcciones
  File _protanopiaInv;
  File _deuteranopiaInv;
  File _tritanopiaInv;

  File _acromatico;
  String fileName;
  String _path;



  Future getPath() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String appDocPath = appDocDir.path;

    setState(() {
      _path = appDocPath;
    });
    setFilters();
  }

  Future setFilters() async {

    File acromatico =  filters.acromatico(_path, _image);
    File deuteranopia =  filters.deuteranopia(_path, _image);
    File monocromaticoAzul =  filters.monocromaticoAzul(_path, _image);
    File tritanopia =  filters.tritanopia(_path, _image);
    File protanopia =  filters.protanopia(_path, _image);

    File proInv = filtersInv.protanopiaInv(_path, _image);
    File triInv = filtersInv.tritanopiaInv(_path, _image);
    File deuInv = filtersInv.deuteranopiaInv(_path, _image);
    setState(() {
      _acromatico = acromatico;
      _deuteranopia = deuteranopia;
      _monocromaticoAzul = monocromaticoAzul;
      _tritanopia = tritanopia;
      _protanopia = protanopia;
      _protanopiaInv = proInv;
      _tritanopiaInv = triInv;
      _deuteranopiaInv = deuInv;
    });

  }


  Future getImage() async {
    var imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    fileName = basename(imageFile.path);
    setState(() {
      _image = imageFile;
      _oimage = imageFile;
    });
    getPath();
  }


  Future setNewImage(File p) async {
    setState(() {
      _oimage = p;
    });
  }


  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        accentColor: Color.fromARGB(255, 60, 60, 0),
      ),
      title: 'seleccionar imagen',
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('seleccionar imagen'),
        ),
        body: new Container(
          color: Color.fromARGB(255, 255, 255, 255),
          alignment: Alignment.topCenter,
            child:Align(
              child : Stack(
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  Container(
                    alignment: _oimage == null? Alignment.center:Alignment.topCenter,
                child: _oimage == null
                ? Text(
                  'No hay imagen selaccionada',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                  )
                : new Image.file(_oimage),
              ),
              Container(
                height: 150.0,
                child:  _image == null
                ? new Text('')
                :ListView(
                padding: EdgeInsets.all(10),
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      print('me hicieron click');
                      setNewImage(_image);
                    },
                    child: Container(
                      width: 100.0,
                      child: Opacity(
                        child: Image.file( _image ),
                        opacity: 0.6,
                      ) ,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      print('me hicieron click');
                      setNewImage(_monocromaticoAzul);
                    },
                    child: Container(
                      width: 100.0,
                      child: Opacity(
                        child: Image.file(_path == null ? _image : _monocromaticoAzul),
                        opacity: 0.6,
                      ) ,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      print('me hicieron click');
                      setNewImage(_acromatico);
                    },
                    child: Container(
                    width: 100.0,
                    child: Opacity(
                      child: Image.file(_path == null ? _image : _acromatico),
                      opacity: 0.6,
                    ) ,
                  ),
                  ),
                  GestureDetector(
                    onTap: () {
                      print('me hicieron click');
                      setNewImage(_deuteranopia);
                    },
                    child: Container(
                    width: 100.0,
                    child: Opacity(
                      child: Image.file(_path == null ? _image : _deuteranopia),
                      opacity: 0.6,
                    ) ,
                  ),
                  ),
                  GestureDetector(
                    onTap: () {
                      print('me hicieron click');
                      setNewImage(_deuteranopiaInv);
                    },
                    child: Container(
                      width: 100.0,
                      child: Opacity(
                        child: Image.file(_path == null ? _image : _deuteranopiaInv),
                        opacity: 0.6,
                      ) ,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      print('me hicieron click');
                      setNewImage(_tritanopia);
                    },
                    child: Container(
                    width: 100.0,
                    child: Opacity(
                      child: Image.file(_path == null ? _image : _tritanopia),
                      opacity: 0.6,
                    ) ,
                  ),
                  ),
                  GestureDetector(
                    onTap: () {
                      print('me hicieron click');
                      setNewImage(_tritanopiaInv);
                    },
                    child: Container(
                      width: 100.0,
                      child: Opacity(
                        child: Image.file(_path == null ? _image : _tritanopiaInv),
                        opacity: 0.6,
                      ) ,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      print('me hicieron click');
                      setNewImage(_protanopia);
                    },
                    child: Container(
                    width: 100.0,
                    child: Opacity(
                      child: Image.file(_path == null ? _image : _protanopia),
                      opacity: 0.6,
                    ) ,
                  ),
                  ),
                  GestureDetector(
                    onTap: () {
                      print('me hicieron click');
                      setNewImage(_protanopiaInv);
                    },
                    child: Container(
                      width: 100.0,
                      child: Opacity(
                        child: Image.file(_path == null ? _image : _protanopiaInv),
                        opacity: 0.6,
                      ) ,
                    ),
                  ),
                ],
              ),
              ),
            ],
          ),
            ),  
        ),
        floatingActionButton: new FloatingActionButton(
          onPressed: getImage,
          tooltip: 'pick image',
          child: new Icon(Icons.camera),
          
        ),
      ),
    );
  }
}
